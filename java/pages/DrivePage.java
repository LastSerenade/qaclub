package pages;

import elements.Button;
import elements.Element;
import elements.Label;
import elements.TextInput;
import org.openqa.selenium.By;

import javax.xml.soap.Text;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static tests.Main.waitInSeconds;

public class DrivePage {

    private Button newButton = new Button(By.xpath("(//button[@aria-label='New'])[1]"));
    private Button selectFolder = new Button(By.xpath("(//span[@class='a-v-j']//div[contains(text(),'Folder')])[1]"));
    private Button createButton = new Button(By.xpath("//button[@name='ok']"));
    private Label checkFolder = new Label(By.xpath("//div[@role='presentation']//span[contains(text(),'Untitled folder')]"));
    private Button moreAction = new Button(By.xpath("(//div[@data-tooltip='More actions'])[1]"));
    private Button renameButton = new Button(By.xpath("//div[@class='a-v-T' and contains(text(),'Rename')]"));
    private TextInput inputName = new TextInput(By.xpath("//input[@class='lb-k-Kk g-Gh']"));
    private Label renamedFolder = new Label(By.xpath("//div[@role='presentation']//span[contains(text(),'Renamed folder')]"));
    private Button deleteButton = new Button(By.xpath("//div[@data-tooltip='Remove']"));
    private Label removedNotification = new Label(By.xpath("//div[@class='la-r']"));
    private Button trashButton = new Button(By.xpath("//div[@aria-label='Trash ']"));
    private Button colorSelector = new Button(By.xpath("//div[contains(text(),'Change color')]"));
    private Button selectRedColor = new Button(By.xpath("//div[@title='Cardinal']"));
    private Label folderCardinalColor = new Label(By.xpath("//div[@aria-label='Untitled folder Google Drive Folder with color Cardinal']"));
    private Button selectAddStar = new Button(By.xpath("//div[contains(text(),'Add star')]"));
    private Label checkStarredFolder = new Label(By.xpath("//div[@aria-label='Starred']"));
    private Button viewDetails = new Button(By.xpath("//div[@aria-label='List view']"));
    private TextInput inputSearch = new TextInput(By.xpath("//input[@placeholder='Search Drive']"));
    private Button searchButton = new Button(By.xpath("//button[@aria-label='Search Google Drive']"));
    private Button checkSearchResults = new Button(By.xpath("//div[@data-target='itemUploadDrop']//div[@aria-label='Untitled folder Google Drive Folder']"));
    private Label checkViewList = new Label(By.xpath("//div[@aria-label='Folder list view']"));
    private Button chooseMoveTo = new Button(By.xpath("//div[contains(text(),'Move to…')]"));
    private Button chosoeNewFolder = new Button(By.xpath("//div[@aria-label='New Folder']"));
    private TextInput newFolderName = new TextInput(By.xpath("//input[@class='e-V-qb-o-vb']"));
    private Button checkButton = new Button(By.xpath("//div[@data-tooltip='Create folder']"));
    private Button openMyDrive = new Button(By.xpath("(//div[@class='a-U-Ze-c-Vf'])[1]"));
    private Button selectNewFolder = new Button(By.xpath("//div[@role='treeitem']//span[@aria-label='MoveToFolder']"));
    private Button selectMoveHere = new Button(By.xpath("//div[contains(text(),'Move here')]"));

    public DrivePage createNewFolder(){
        newButton.click();
        selectFolder.click();
        createButton.click();
        return this;
    }

    public DrivePage renameNewFolder(){
        checkFolder.waitAndClick();
        waitInSeconds(1);
        moreAction.click();
        renameButton.waitAndClick();
        inputName.inputText("Renamed folder");
        createButton.click();
        return this;
    }

    public DrivePage removeFolder(){
        checkFolder.waitAndClick();
        deleteButton.click();
        return this;
    }

    public DrivePage selectColor(){
        checkFolder.waitAndClick();
        waitInSeconds(1);
        moreAction.click();
        waitInSeconds(1);
        colorSelector.click();
        selectRedColor.waitAndClick();
        return this;
    }

    public DrivePage addStartoFolder(){
        checkFolder.waitAndClick();
        waitInSeconds(1);
        moreAction.click();
        waitInSeconds(1);
        selectAddStar.click();
        //viewDetails.click();
        return this;
    }

    public DrivePage searchFolder(){
        inputSearch.inputText("Untitled folder");
        waitInSeconds(1);
        searchButton.click();
        return this;
    }

    public DrivePage selectViewDetails(){
        viewDetails.click();
        checkViewList.waitForElementToBeVisible();
        return this;
    }

    public DrivePage selectMoveTo(){
        checkFolder.waitAndClick();
        waitInSeconds(1);
        moreAction.click();
        chooseMoveTo.waitAndClick();
        waitInSeconds(1);
        chosoeNewFolder.click();
        newFolderName.inputText("MoveToFolder");
        waitInSeconds(1);
        checkButton.click();
        waitInSeconds(1);
        selectMoveHere.click();
        openMyDrive.click();
        selectNewFolder.click();
        return this;
    }



    public void verifyViewList(){
        waitInSeconds(1);
        assertTrue(checkViewList.isPresent());
    }

    public void verifySearchResult(){
        waitInSeconds(1);
        assertTrue(checkSearchResults.isPresent());
    }

    public void verifyStarredFolder(){
        waitInSeconds(1);
        assertTrue(checkStarredFolder.isPresent());
    }

    public void verifyFolderColorIsRed(){
        waitInSeconds(1);
        assertTrue(folderCardinalColor.isPresent());
    }

    public void verifyFolder(){
        waitInSeconds(2);
        assertTrue(checkFolder.isPresent());
    }

    public void verifyRenamedFolder(){
        waitInSeconds(2);
        assertTrue(renamedFolder.isPresent());
    }

    public void verifyFolderRemoved(){
        waitInSeconds(2);
        assertTrue(removedNotification.isPresent());
    }

    public TrashPage goToTrash() {
        waitInSeconds(1);
        trashButton.click();
        return new TrashPage();
    }
}