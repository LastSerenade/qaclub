package pages;

import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static org.testng.AssertJUnit.assertTrue;
import static tests.Main.waitInSeconds;

public class TrashPage {

    private Button restoreButton = new Button(By.xpath("//div[@data-tooltip='Restore from trash']"));
    private Label restoredNotification = new Label(By.xpath("//div[@class='la-r' and contains(text(), 'Restored Untitled folder')]"));
    private Label checkFolder = new Label(By.xpath("//div[@role='presentation']//span[contains(text(),'Untitled folder')]"));
    private Label verifyDeletePopup = new Label(By.xpath("//span[contains(text(),'Delete forever?')]"));
    private Button deleteForeverButton = new Button(By.xpath("//button[@class='h-De-Vb h-De-Y']"));
    private Label permanentlyDeleted = new Label(By.xpath("//span[@class='a-la-ua-x']"));
    private Button deleteButton = new Button(By.xpath("//div[@aria-label='Delete forever']"));


    public TrashPage restoreFolder(){
        checkFolder.waitAndClick();
        restoreButton.click();
        return this;
    }

    public TrashPage deleteForever(){
        checkFolder.waitAndClick();
        deleteButton.click();
        verifyDeletePopup.isPresent();
        deleteForeverButton.click();
        return this;
    }

    public void verifyFolderRestored(){
        waitInSeconds(2);
        assertTrue(restoredNotification.isPresent());
    }

    public void verifyFolderDeletedPermanently(){
        waitInSeconds(1);
        assertTrue(permanentlyDeleted.isPresent());
    }


}
