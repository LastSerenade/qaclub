package pages;

import elements.Button;
import org.openqa.selenium.By;
import static pages.LoginPage.loginPageisShown;
import static tests.Main.waitInSeconds;
import static tests.Main.getDriver;

public class MainPage {

    private Button navigationButton = new Button(By.xpath("//a[@class='gb_b gb_6b']"));
    private Button moreButton = new Button(By.xpath("//a[@class='gb_ja gb_Hf']"));
    private Button doccumentsButton = new Button(By.xpath("//span[text()='Docs']"));
    private Button gmailButton = new Button(By.xpath("//a[@class='gb_P']"));
    private Button signIn = new Button(By.xpath("//a[@class='gb_Lf gb_Fa gb_yb']"));
    private Button driveButton = new Button(By.xpath("//a[@id='gb49']"));//span[text()='Drive']


    public void goToGmail() {
        gmailButton.click();
    }


    public DocumentsPage openGmail() {
        signIn.click();
        if (loginPageisShown()) {
            new LoginPage().login();
        }
        navigationButton.waitAndClick();
        moreButton.click();
        waitInSeconds(1);
        doccumentsButton.click();
        return new DocumentsPage();
    }

    public DrivePage openDrive() {
        signIn.click();
        if (loginPageisShown()) {
            new LoginPage().login();
        }
        navigationButton.click();
        driveButton.waitAndClick();
        return new DrivePage();

    }

    public InboxPage goToEmail() {
        getDriver().get("http://mail.google.com");
        if (loginPageisShown()) {
            new LoginPage().login();
        }
        return new InboxPage();
    }

}



