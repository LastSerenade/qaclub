package pages;

        import elements.Button;
        import elements.Element;
        import elements.TextInput;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebElement;
        import static tests.Main.waitInSeconds;

public class LoginPage {

    private static TextInput emailInput = new TextInput(By.id("identifierId"));
    private TextInput passwordInput = new TextInput(By.cssSelector("input[type='password']"));
    private Button nextButton = new Button(By.xpath("//span[@class='RveJvd snByac']"));


    public void login(){
        emailInput.inputText("explosionarthur@gmail.com");
        nextButton.click();
        waitInSeconds(1);
        passwordInput.inputText("aycaramba");
        nextButton.click();
    }

    public static boolean loginPageisShown() {
        return emailInput.isPresent();
    }



}
