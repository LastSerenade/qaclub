package pages;

import elements.Button;
import elements.TextInput;
import elements.CheckMail;
import org.openqa.selenium.By;
import conf.EmailConfig.EmailData;
import conf.EmailConfig.EmailService;

import java.util.List;

import static org.testng.AssertJUnit.assertTrue;
import static tests.Main.waitInSeconds;

public class InboxPage {

    private Button composeButton = new Button(By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']"));
    private TextInput toField = new TextInput(By.cssSelector("[aria-label='To']"));
    private TextInput subjectField = new TextInput(By.cssSelector("input[name='subjectbox']"));
    private TextInput messageArea = new TextInput(By.xpath("//div[@class='Am Al editable LW-avf']"));
    private Button sendButton = new Button(By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3']"));
    private CheckMail emailVerification = new CheckMail(By.xpath("//span[@class='bog']/..//span[contains(text(),'HelloMessage')]"));



    public InboxPage composeMail(){
        composeButton.click();
        toField.inputText("explosionarthur@gmail.com");
        subjectField.inputText("qatest");
        messageArea.inputText("HelloMessage");
        sendButton.click();
        return this;
    }
    public void verifyMail(){
        waitInSeconds(10);
        assertTrue(emailVerification.isPresent());
    }

    public InboxPage verifyEmailReceived() {
        List<EmailData> emails = EmailService.connectAndGetAllEmailsFromFolder("Inbox");
        EmailService.waitForEmailAndRemoveAllMessagesFromFolder("Inbox");
        return this;
    }

    public InboxPage sendEmail() {
        composeButton.click();
        toField.inputText("explosionarthur@gmail.com");
        subjectField.inputText("Subject");
        sendButton.click();
        return this;
    }

}
