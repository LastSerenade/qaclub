package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class ChangeColor extends Main {

    @Test
    public void selectColor(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.createNewFolder()
                .selectColor()
                .verifyFolderColorIsRed();
    }
}
