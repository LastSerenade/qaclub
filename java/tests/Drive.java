package tests;


import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class Drive extends Main {

    @Test
    public void createDriveFolder(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.createNewFolder()
                .verifyFolder();
    }
}
