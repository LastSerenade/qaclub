package tests;

import org.junit.Test;
import pages.LoginPage;
import pages.InboxPage;
import pages.MainPage;

public class SendMail extends Main {

    @Test
    public void sendEmail() {
        MainPage mainPage = new MainPage();
        LoginPage loginPage = new LoginPage();
        InboxPage inboxPage = new InboxPage();
        mainPage.goToGmail();
        loginPage.login();
        inboxPage.composeMail();
        inboxPage.verifyMail();
    }
}