package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class CalendarTest {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 365);
        Date nextYear = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.ENGLISH);
        String currentDay = dateFormat.format(today);
        String nextYearDay = dateFormat.format(nextYear);
        System.out.println("Current date and time is " +currentDay);
        System.out.println("Exactly one year from now will be " +nextYearDay);
    }
}
