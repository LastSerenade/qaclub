package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class MoveTo extends Main {

    @Test
    public void moveTo(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.createNewFolder()
                .selectMoveTo()
                .verifyFolder();

    }
}
