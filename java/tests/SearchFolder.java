package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class SearchFolder extends Main {

    @Test
    public void searchFolder(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(3);
        drivePage.createNewFolder()
                .searchFolder()
                .verifySearchResult();
    }
}
