package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class AddStar extends Main {

    @Test
    public void addStarToFolder(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(3);
        drivePage.createNewFolder()
                .addStartoFolder()
                .verifyStarredFolder();

    }
}