package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class ViewDetails extends Main {

    @Test
    public void viewDetailsList(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.selectViewDetails()
                .removeFolder()
                .verifyViewList();
    }
}
