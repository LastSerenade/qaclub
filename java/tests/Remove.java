package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class Remove extends Main {

    @Test
    public void removeDriveFolder(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.createNewFolder()
                .removeFolder()
                .verifyFolderRemoved();

    }
}
