package tests;


import org.testng.annotations.Test;
import pages.DrivePage;
import pages.MainPage;

public class Rename extends Main {

    @Test

    public void renameDriveFolder(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.createNewFolder()
                .renameNewFolder()
                .verifyRenamedFolder();

    }
}
