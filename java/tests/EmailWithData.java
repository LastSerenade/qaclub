package tests;

import org.junit.Test;
import pages.MainPage;


public class EmailWithData extends Main {

    @Test
    public void emailTest(){
        MainPage mainPage = new MainPage();
        mainPage.goToEmail()
                .composeMail()
                .verifyEmailReceived();
    }
}