package tests;

import org.junit.Test;
import pages.DrivePage;
import pages.MainPage;

public class Restore extends Main {

    @Test
    public void restoreDriveFolder(){
        MainPage mainPage = new MainPage();
        DrivePage drivePage = new DrivePage();
        mainPage.openDrive();
        waitInSeconds(4);
        drivePage.createNewFolder()
                .removeFolder()
                .goToTrash()
                .restoreFolder()
                .verifyFolderRestored();

    }
}
