package elements;

import static tests.Main.getDriver;

import org.openqa.selenium.*;
import com.google.common.base.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class Element {
    protected By by;

    public Element(By by){
        this.by = by;
    }
    protected WebElement composeWebElement(){
        return getDriver().findElement(by);
    }

    public void waitForElementToBeVisible(){
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitAndClick() {
        new FluentWait(getDriver())
                .withTimeout(30000, TimeUnit.MILLISECONDS)
                .pollingEvery(200, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .withMessage("Element found by " + by + " is still invisible, but should not be")
                .until(new Function<WebDriver, Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        try{
                            getDriver().findElement(by).click();
                            return true;
                        }catch(WebDriverException ignored){
                        }
                        return false;
                    }
                }); }

    public boolean isPresent() {
        try {
            try {
                composeWebElement().isDisplayed();
            } catch (NoSuchElementException e) {
                return false;
            }
        }catch (StaleElementReferenceException e){
            composeWebElement().isDisplayed();
        }
        return true;
    }

    public void scrollToElement() {
        try {
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", composeWebElement());
        } catch (StaleElementReferenceException ignore) {
            //ignore this exception
        }
    }




}
