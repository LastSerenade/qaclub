package elements;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class Button extends Element {

    public Button(By by) {
        super(by);
    }

        public void click(){
            composeWebElement().click();
    }


}
